package com.example.administrator.ardemo.panorama;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.administrator.ardemo.Adapter;
import com.example.administrator.ardemo.R;
import com.example.administrator.ardemo.panorama.image.ImageActivity;
import com.example.administrator.ardemo.panorama.video.VideoActivity;

/**
 * Google全景图查看器
 */
public class PanoramaActivity extends AppCompatActivity implements Adapter.OnItemClickListener {
    RecyclerView rv;

    Adapter adapter;

    String[] printers = new String[]{"360度全景图片", "360度全景视频"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panorama);

        adapter = new Adapter(this, printers);
        adapter.setOnItemClickListener(this);
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = null;
        switch (position) {
            case 0://360度全景图片
                intent = new Intent(this, ImageActivity.class);
                break;
            case 1://360度全景视频
                intent = new Intent(this, VideoActivity.class);
                break;
        }
        startActivity(intent);
    }

}