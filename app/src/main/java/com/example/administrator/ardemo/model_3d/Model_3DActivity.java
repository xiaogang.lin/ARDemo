package com.example.administrator.ardemo.model_3d;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.administrator.ardemo.R;
import com.study.xuan.gifshow.widget.stlview.callback.OnReadCallBack;
import com.study.xuan.gifshow.widget.stlview.widget.STLView;
import com.study.xuan.gifshow.widget.stlview.widget.STLViewBuilder;

/**
 * 3D模型查看器
 */
public class Model_3DActivity extends AppCompatActivity {
    STLView mStl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_3_d);

        mStl = findViewById(R.id.stl);
        STLViewBuilder.init(mStl).Assets(this, "BelleBook_Big.stl").build();
        mStl.setTouch(true);
        mStl.setScale(true);
        mStl.setRotate(true);
        mStl.setSensor(true);
        mStl.setOnReadCallBack(callBack);
    }

    OnReadCallBack callBack = new OnReadCallBack() {

        @Override
        public void onStart() {

        }

        @Override
        public void onReading(final int cur, final int total) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(Model_3DActivity.this, "加载进度：" + cur, Toast.LENGTH_SHORT).show();
                }
            });

        }

        @Override
        public void onFinish() {
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStl.setOnReadCallBack(null);
        mStl.delete();
    }
}
