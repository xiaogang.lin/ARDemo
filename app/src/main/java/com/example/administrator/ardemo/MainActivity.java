package com.example.administrator.ardemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.administrator.ardemo.gif.GifActivity;
import com.example.administrator.ardemo.model_3d.Model_3DActivity;
import com.example.administrator.ardemo.panorama.PanoramaActivity;

public class MainActivity extends AppCompatActivity implements Adapter.OnItemClickListener {
    RecyclerView rv;

    Adapter adapter;

    String[] printers = new String[]{"GIF全景图查看器", "3D模型查看器", "Google全景图查看器"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new Adapter(this, printers);
        adapter.setOnItemClickListener(this);
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = null;
        switch (position) {
            case 0://GIF全景图查看器
                intent = new Intent(this, GifActivity.class);
                break;
            case 1://3D模型查看器
                intent = new Intent(this, Model_3DActivity.class);
                break;
            case 2://Google全景图查看器
                intent = new Intent(this, PanoramaActivity.class);
                break;
        }
        startActivity(intent);
    }
}