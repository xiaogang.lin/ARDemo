package com.example.administrator.ardemo.gif;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.administrator.ardemo.R;
import com.study.xuan.gifshow.gif.VrGifView;

/**
 * GIF全景图查看器
 */
public class GifActivity extends AppCompatActivity {
    VrGifView mGif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif);

        mGif = (VrGifView) findViewById(R.id.gif);
        mGif.setTouch(true);//是否可触摸
        mGif.setDrag(true);//是否可拖拽
        mGif.setScale(false);//是否可伸缩
        mGif.setMoveMode(VrGifView.MODE_FAST);//触摸响应速度
    }
}
