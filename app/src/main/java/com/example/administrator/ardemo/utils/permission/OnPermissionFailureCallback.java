package com.example.administrator.ardemo.utils.permission;

public interface OnPermissionFailureCallback {

    void call(String[] permission);
}
