package com.example.administrator.ardemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Adapter extends RecyclerView.Adapter<Adapter.MyHolder> {
    private Context context;
    private String[] list;
    private OnItemClickListener listener;

    public Adapter(Context context, String[] list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(context).inflate(R.layout.item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        holder.tvPrinter.setText(list[position]);
        holder.tvPrinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    class MyHolder extends RecyclerView.ViewHolder {
        private TextView tvPrinter;

        public MyHolder(View itemView) {
            super(itemView);
            tvPrinter = (TextView) itemView.findViewById(R.id.tv_printer);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
