package com.example.administrator.ardemo.utils.image;

import android.graphics.Bitmap;

public class ImageEntity {
    //图片路径：可能是网络图片，可能是本地图片
    public String path;

    //根据图片路径转换成位图
    public Bitmap bitmap;

    //是否是网络图片
    public boolean isNetWork;

    //图片所在位置
    public int position;
}
