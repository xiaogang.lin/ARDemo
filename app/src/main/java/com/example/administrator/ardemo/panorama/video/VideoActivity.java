package com.example.administrator.ardemo.panorama.video;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.administrator.ardemo.R;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.google.vr.sdk.widgets.video.VrVideoEventListener;
import com.google.vr.sdk.widgets.video.VrVideoView;

import java.io.IOException;

/**
 * 360度全景视频
 */
public class VideoActivity extends AppCompatActivity{
    private static final String TAG = "VrPanorama";

    private String videoUri = "congo.mp4";//assets文件夹下的文件名
    private VrVideoView.Options options = new VrVideoView.Options();
    private VideoLoaderTask backgroundVideoLoaderTask;
    private VrVideoView videoWidgetView;
    private boolean isPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        videoWidgetView = (VrVideoView) findViewById(R.id.video_view);
        videoWidgetView.setEventListener(new ActivityVideoEventListener());
        options.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER;
        //TYPE_STEREO_OVER_UNDER立体模式
        //FORMAT_DEFAULT默认格式（SD卡或assets）
        //FORMAT_HLS流媒体数据格式（直播）
        options.inputFormat = VrVideoView.Options.FORMAT_DEFAULT;
        if (backgroundVideoLoaderTask != null)
            backgroundVideoLoaderTask.cancel(true);
        backgroundVideoLoaderTask = new VideoLoaderTask();
        backgroundVideoLoaderTask.execute(videoUri);

    }

    private class ActivityVideoEventListener extends VrVideoEventListener {

        @Override
        public void onLoadSuccess() {//加载成功
            Log.i(TAG, "Sucessfully loaded video " + videoWidgetView.getDuration());
        }

        @Override
        public void onLoadError(String errorMessage) {//加载失败
            Log.e(TAG, "Error loading video: " + errorMessage);
        }

        @Override
        public void onClick() {//当我们点击了VrVideoView时候触发
            if (isPaused) {
                videoWidgetView.playVideo();//播放
            } else {
                videoWidgetView.pauseVideo();//暂停
            }
            isPaused = !isPaused;
        }

        @Override
        public void onNewFrame() {//一个新的帧被绘制到屏幕上。
        }

        @Override
        public void onCompletion() {//视频播放完毕。
            videoWidgetView.seekTo(0);//移动到视频开始
        }
    }

    /**
     * 异步任务加载视频
     */
    class VideoLoaderTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... uri) {
            try {
                videoWidgetView.loadVideoFromAsset(uri[0], options);//加载视频文件
            } catch (IOException e) {//视频文件打开失败
                Log.e(TAG, "Could not open video: " + e);
            }
            return true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoWidgetView.pauseRendering();//暂停3D渲染和跟踪
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoWidgetView.resumeRendering();//恢复3D渲染和跟踪，但官方文档上面没有写
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoWidgetView.shutdown();//关闭渲染并释放相关的内存
        if (backgroundVideoLoaderTask != null)
            backgroundVideoLoaderTask.cancel(true);//停止异步任务
    }
}
